Projekt składający się z 5 zadań.
1. Łamanie haseł metodą brute-force.
2. Łamanie haseł metodą słownikową.
3. Analiza ruchu HTTP
4. Analiza ruchu SSH
5. Analiza ruchu FTP

Wszystkie czynności są opisane krok po kroku.
